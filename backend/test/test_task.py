import imp
import unittest
import sys
from src.domain.task import task


class TaskTest(unittest.TestCase):

    def test_task_can_be_constructed(self):
        a_simple_task = task.Task("Pass the test")
        self.assertIsNotNone(a_simple_task)   


if __name__ == '__main__':
    unittest.main()